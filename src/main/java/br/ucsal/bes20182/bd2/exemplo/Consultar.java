package br.ucsal.bes20182.bd2.exemplo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.ucsal.bes20182.bd2.domain.Administrativo;
import br.ucsal.bes20182.bd2.domain.Cidade;
import br.ucsal.bes20182.bd2.domain.Endereco;
import br.ucsal.bes20182.bd2.domain.Estado;
import br.ucsal.bes20182.bd2.domain.Funcionario;
import br.ucsal.bes20182.bd2.domain.Vendedor;
import br.ucsal.bes20182.bd2.domain.PessoaJuridica;
import br.ucsal.bes20182.bd2.domain.RamoAtividade;
import br.ucsal.bes20182.bd2.enums.SituacaoVendedorEnum;

public class Consultar {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("pu1");
			EntityManager em = emf.createEntityManager();

			PopularBanco  popular = new PopularBanco();
			popular.popularBase(em);

			em.clear();
			
			List<PessoaJuridica> pessoasJuridicas=consultaClientesPorVendedor(em, "Ruy");
			List<Vendedor> vendedores=consultaRamosAtividade(em, "Industrial");
			List<Funcionario> funcionarios =consultaFuncionariosAtivos(em, SituacaoVendedorEnum.ATIVO);
			List<Estado> estados=consultaEstadoSemCidade(em);
			consultaQuantidadeVendedoresCidade(em);
			List<Vendedor> vendedores1=consultaVendedoresDaCidade(em, "Salvador");


			
			
			
		
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}
	private static List<PessoaJuridica> consultaClientesPorVendedor(EntityManager em, String nome_vendedor) {
		em.getTransaction().begin();
		Query query = em.createQuery("select pj.nome from PessoaJuridica pj join pj.vendedores v where v.nome=:nome_vendedor");
		query.setParameter("nome_vendedor", nome_vendedor);
		List<PessoaJuridica> pessoasJuridicas = query.getResultList();
		em.getTransaction().commit();
		return pessoasJuridicas;
	}
		
	private static List<Vendedor> consultaRamosAtividade(EntityManager em, String nome_ramo) {
		em.getTransaction().begin();
		Query query = em.createQuery("select v from Vendedor v join fetch v.telefones join v.clientes c join c.ramosAtividade r where r.nome=:nome_ramo");
		query.setParameter("nome_ramo", nome_ramo);
		List<Vendedor> vendedores = query.getResultList();
		em.getTransaction().commit();
	return vendedores;
	}

	private static  List<Funcionario> consultaFuncionariosAtivos(EntityManager em, SituacaoVendedorEnum situacao) {
		em.getTransaction().begin();
		Query query = em.createQuery("select f.cpf, f.nome from Funcionario f where f.situacao=:situacao");
		query.setParameter("situacao", situacao);
		List<Funcionario> funcionarios = query.getResultList();
		em.getTransaction().commit();
		return funcionarios;
	}
	

	private static List<Estado> consultaEstadoSemCidade(EntityManager em) {
		em.getTransaction().begin();
		Query queryEstados = em.createQuery("select e from Estado e left join e.cidades c where c is null");
		List<Estado> estados = queryEstados.getResultList();
		em.getTransaction().commit();
		return estados;
	}

	private static  List<Object[]> consultaQuantidadeVendedoresCidade(EntityManager em) {
		return em.createQuery("select c, count(v) from Vendedor v join v.endere�o.cidade c group by c").getResultList();
	}
	
	private static List<Vendedor> consultaVendedoresDaCidade(EntityManager em, String nome_cidade) {
		em.getTransaction().begin();
		Query query = em.createQuery("select v from Vendedor v join fetch v.telefones join v.endere�o.cidade c where c.nome=:nome_cidade");
		query.setParameter("nome_cidade", nome_cidade);
		List<Vendedor> vendedores = query.getResultList();
		em.getTransaction().commit();
		return vendedores;
	}

}
