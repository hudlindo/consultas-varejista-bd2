package br.ucsal.bes20182.bd2.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20182.bd2.enums.SituacaoVendedorEnum;

public class SituacaoVendedorConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoVendedorEnum attribute) {
		return attribute.getCodigo();
	}

	@Override
	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		return SituacaoVendedorEnum.valueOfCodigo(dbData);
	}

}
