package br.ucsal.bes20182.bd2.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	@Column(name = "cnpj", columnDefinition = "char(11)")
	private String cnpj;

	@Column(name = "nome", length = 40, nullable = false)
	private String nome;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="tab_pessoa_juridica_ramo_atividade")
	private List<RamoAtividade> ramosAtividade;

	@Column(name = "faturamento", columnDefinition = "numeric(10,2)", nullable = false)
	private Double faturamento;

	@ManyToMany(mappedBy="clientes")
	private List<Vendedor> vendedores;

	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramosAtividade, Double faturamento,
			List<Vendedor> vendedores) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramosAtividade = ramosAtividade;
		this.faturamento = faturamento;
		this.vendedores = vendedores;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", nome=" + nome + ", ramosAtividade=" + ramosAtividade
				+ ", faturamento=" + faturamento + ", vendedores=" + vendedores + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((faturamento == null) ? 0 : faturamento.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((ramosAtividade == null) ? 0 : ramosAtividade.hashCode());
		result = prime * result + ((vendedores == null) ? 0 : vendedores.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (faturamento == null) {
			if (other.faturamento != null)
				return false;
		} else if (!faturamento.equals(other.faturamento))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (ramosAtividade == null) {
			if (other.ramosAtividade != null)
				return false;
		} else if (!ramosAtividade.equals(other.ramosAtividade))
			return false;
		if (vendedores == null) {
			if (other.vendedores != null)
				return false;
		} else if (!vendedores.equals(other.vendedores))
			return false;
		return true;
	}
	
}
